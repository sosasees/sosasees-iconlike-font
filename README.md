> ⚠ this font is a failed project:
> i sized the icons to text but to not take up too&nbsp;much space, i
> should size icons to text instead.<br>
> my next font
> [poppy ribbon desktop](https://codeberg.org/sosasees/poppy-ribbon-desktop-font)
> is more appropriately sized

# sosasees iconlike font

made in [FontForge](https://fontforge.org).
licensed under [SIL OFL 1.1](https://openfontlicense.org/open-font-license-official-text/).

![example](.readme-pictures/example.webp)

available glyphs are listed under [/characters](/characters).

## distinct symbols

for easier reading, all letters in this font should look
as different from each other as possible.

for lowercase **r** and **s** this is not possible
without making them look unusual:
- **r** looks different from **dotless i**
- **s** looks different from **five**

so uppercase letters look different from lowercase,
their leftmost side is 1dp thicker.

## measurements

sosasees iconlike is made with similar size restrictions as
[Standard Material icons](https://m3.material.io/styles/icons/designing-icons#138a40d6-fdd0-4d9b-a6a0-1dc1c703850e):

![](.readme-pictures/measurements.webp)

- all symbols are 24dp tall, with 2dp padding on the top and bottom
   - in the useable 20dp height, ascent and descent are 5dp
   - the rest is 10dp
- all symbols are 2dp thick
   - except the leftmost side of uppercase letters, which is 3dp
- symbols should look like they are 2dp apart from each other
   - not all of them do because it would take _way too long_
     to adjust kerning of the whole font
- some symbols might break these rules when it makes them better

### underlines

underlines are placed directly under the 24dp height of the symbols.
since the bottom 2dp are never used, this leaves a 2dp gap
between the symbols and the underline.

to account for this underline, you will have to add 2dp extra
line spacing to text with underline.

![](.readme-pictures/measurements_underline.webp)
