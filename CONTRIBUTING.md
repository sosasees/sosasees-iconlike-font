# contributing guidelines

## contents

- CODE OF CONDUCT
- commit titles and messages
	- line length
	- signature

## CODE OF CONDUCT

- please be nice and respectful of each other
- please be polite
- please don't be aggressive
- please keep criticism constructive
- please act how is reasonable in a professional setting

if you don't follow these rules, you will:
- if first offense, get a warning
- if second offense or more, get banned temporarily or forever
  from this project

## commit titles and messages

### line length

please break lines after 72 characters.
shrinking your terminal to 72 characters wide really helps 🙂 .

### signature

please sign your commits in one of these ways,
so your authorship will not get lost when rebasing:

> with your user link:
> 
> ```
> 	— <https://codeberg.org/sosasees>
> ```

> with your e-mail address:
> 
> ```
> 	— <mailto:sosasees@protonmail.com>
> ```

you can also type a nickname before the link if you want:

> ```
> 	— sosasees <https://codeberg.org/sosasees>
> ```
> or
> ```
> 	— sosasees <mailto:sosasees@protonmail.com>
> ```

please begin your signature with these special characters:

1. (optional) U+0009 ``	`` [Tab] character tabulation
2. U+2014 ``—`` em dash
3. U+2020 `` `` [Space] space

you can copy-paste the special characters from the examples above.
