## required ligatures

single glyphs for two characters in a row
which must be used instead of the glyphs for the single characters
to properly render the script.
they can range from just smoothing messy connections in Latin script
to making Arabic script readable.

first letter               | second letter               | purpose
---------------------------|-----------------------------|----------------------------------------------------------------------------------
U+0069 ATIN SMALL LETTER I | U+0066 LATIN SMALL LETTER F | so the letters 'if' in words like if-else and iffy don't intersect in a messy way
