## contextual substitutions

character variants used in certain situations
to make the text look right
when kerning isn't enough

original                    | substitution | context
----------------------------|--------------|----------------------------------
U+009e LATIN SMALL LETTER N | n.after_i    | after U+0069 LATIN SMALL LETTER I
